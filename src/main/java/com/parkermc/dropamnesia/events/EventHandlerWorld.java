package com.parkermc.dropamnesia.events;

import com.parkermc.dropamnesia.droptools.DropManager;

import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventHandlerWorld {
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event) {
		if(!event.getWorld().isRemote) {
			DropManager.instance = new DropManager(event.getWorld());
			DropManager.instance.load();
		}
	}
}
