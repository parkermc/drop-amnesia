package com.parkermc.dropamnesia.events;

import com.parkermc.amnesia.events.AmnesiaEvent;
import com.parkermc.amnesia.events.IAmnesiaEvents;
import com.parkermc.dropamnesia.droptools.DropManager;

@AmnesiaEvent
public class AmnesiaEvents implements IAmnesiaEvents{
	
	@Override
	public void normal() {
		DropManager.instance.setNormal();
	}

	@Override
	public void random() {
		DropManager.instance.randomise();
	}

	@Override
	public void updatePost() {		
	}

	@Override
	public void updatePostClient() {
	}

	@Override
	public void cure() {
	}
}