package com.parkermc.dropamnesia.events;

import java.util.ArrayList;
import java.util.List;

import com.parkermc.amnesia.ModTools;
import com.parkermc.dropamnesia.droptools.DropManager;
import com.parkermc.dropamnesia.droptools.ResourceLocationMeta;
import com.parkermc.dropamnesia.proxy.ProxyCommon;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class EventBlockHarvest {
	List<BlockPos> drops = new ArrayList<BlockPos>();
	
	@SuppressWarnings("deprecation")
	@SubscribeEvent
	public void onBlockHarvest(BlockEvent.HarvestDropsEvent event) {
		if(!drops.contains(event.getPos())) {
			ResourceLocationMeta dropBlockLocation = DropManager.instance.getDropBlock(new ResourceLocationMeta(event.getState()));
			if(dropBlockLocation != null&&ForgeRegistries.BLOCKS.containsKey(dropBlockLocation.getResourceLocation())) {
				Block dropBlock = ForgeRegistries.BLOCKS.getValue(dropBlockLocation.getResourceLocation());
				if(!(event.isSilkTouching()&&!ProxyCommon.config.silk_touch_drop_random)) {
					IBlockState dropState = dropBlock.getDefaultState();
					if(dropBlock.getMetaFromState(dropState) != dropBlockLocation.getMetadata()) {
						dropState = dropBlock.getStateFromMeta(dropBlockLocation.getMetadata());
					}
					
					if(dropBlock.getMetaFromState(dropState) != dropBlockLocation.getMetadata()) {
						ModTools.logError("Can't get correct meta data id for block %s probably due to that mod's programming", dropBlockLocation.toString());
					}
					drops.add(event.getPos());
					if (event.getHarvester()==null){
						dropBlock.dropBlockAsItemWithChance(event.getWorld(), event.getPos(), dropState, event.getDropChance(), event.getFortuneLevel());
					}else {
						dropBlock.harvestBlock(event.getWorld(), event.getHarvester(), event.getPos(), dropState, null, event.getHarvester().getActiveItemStack());
					}
					drops.remove(event.getPos());
					event.getDrops().clear();
				}
			}
		}
	}
}
