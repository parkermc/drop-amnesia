package com.parkermc.dropamnesia.proxy;

import java.io.File;
import java.io.IOException;

import com.parkermc.amnesia.ModTools;
import com.parkermc.dropamnesia.Config;
import com.parkermc.dropamnesia.ModMain;
import com.parkermc.dropamnesia.events.EventHandlerWorld;
import com.parkermc.dropamnesia.events.EventBlockHarvest;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ProxyCommon {
	public static Config config = new Config();
	public static File configFile;
	private EventHandlerWorld eventHandlerWorldLoad = new EventHandlerWorld();
	private EventBlockHarvest eventLootTableLoad = new EventBlockHarvest();
	
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(eventHandlerWorldLoad);
		MinecraftForge.EVENT_BUS.register(eventLootTableLoad);
		configFile = new File(event.getModConfigurationDirectory(), ModMain.MODID+".json");
		try {
			config = Config.Read(configFile);
		} catch (IOException e) {
			ModTools.logError("Error loading config resetting to defaults");
			e.printStackTrace();
		}
	}
	
	public void init(FMLInitializationEvent event) {
	}
	
	public void postInit(FMLPostInitializationEvent event) {
	}
}
