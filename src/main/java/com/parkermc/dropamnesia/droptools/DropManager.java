package com.parkermc.dropamnesia.droptools;

import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.parkermc.amnesia.ModTools;
import com.parkermc.dropamnesia.proxy.ProxyCommon;

import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class DropManager{
	private static String folder = "data";
	private static String file = "drop_amnesia.dat";
	
	public static DropManager instance;
	
	private World world;
	public Map<ResourceLocationMeta,ResourceLocationMeta> dropMapper = new HashMap<ResourceLocationMeta,ResourceLocationMeta>();
	public boolean normal = false;
	
	public DropManager() {	
	}
	
	public DropManager(World world) {
		this.world = world;
	}
	
	public ResourceLocationMeta getDropBlock(ResourceLocationMeta resourceLocation) {
		if((!this.normal)&&this.dropMapper.containsKey(resourceLocation)) {
			return this.dropMapper.get(resourceLocation);
		}
		return null;
	}
	
	public boolean load() {
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			
			if(filename.exists()) {
				try {
	                FileInputStream fileinputstream = new FileInputStream(filename);
	                NBTTagCompound nbttagcompound = CompressedStreamTools.readCompressed(fileinputstream);
	                fileinputstream.close();
	                this.readFromNBT(nbttagcompound.getCompoundTag("data"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return true;
			}else {
				this.randomise();
				this.save();
				return false;
			}
		}
		return false;
	}

	public void randomise() {
		this.dropMapper.clear();
		this.normal = false;
		Random random = new Random();
		for(List<String> group : ProxyCommon.config.groups) {
			if(!group.contains("_example")) {
				List<ResourceLocationMeta> to_randomise = new ArrayList<ResourceLocationMeta>();
				for(String i : group) {
					ResourceLocationMeta location = new ResourceLocationMeta(i);
					if(ForgeRegistries.BLOCKS.containsKey(location.getResourceLocation())) {
						if(!this.dropMapper.containsKey(location)&&!to_randomise.contains(location)) {
							to_randomise.add(location);
						}else {
							ModTools.logError("block \"%s\" is listed twice" , location.toString());
						}
					}else {
						ModTools.logError("can't find block: " + location.toString());
					}
				}
				
				List<ResourceLocationMeta> map_to = new ArrayList<ResourceLocationMeta>(to_randomise);
				for(ResourceLocationMeta i : to_randomise) {
					this.dropMapper.put(i, map_to.remove(random.nextInt(map_to.size())));
				}
			}
		}
		
		// Group the things
		for(String parent_block : ProxyCommon.config.block_grouping.keySet()) {
			if(!parent_block.equals("_")) {
				ResourceLocationMeta parent_location = new ResourceLocationMeta(parent_block);
				if(ForgeRegistries.BLOCKS.containsKey(parent_location.getResourceLocation())) {
					for(String child_block : ProxyCommon.config.block_grouping.get(parent_block)) {
						ResourceLocationMeta child_location = new ResourceLocationMeta(child_block);
						if(ForgeRegistries.BLOCKS.containsKey(child_location.getResourceLocation())) {
							if(this.dropMapper.containsKey(parent_location)) {
								this.dropMapper.put(child_location, this.dropMapper.get(parent_location));
							}else {
								ModTools.logError("can't find block \"%s\" in a group", parent_location.toString());
							}
						}else {
							ModTools.logError("can't find block: " + child_location.toString());
						}
					}
				
				}else {
					ModTools.logError("can't find block: " + parent_location.toString());
				}
			}
		}
		this.save();
	}
	
	public void readFromNBT(NBTTagCompound nbt) {
		this.normal = nbt.getBoolean("normal");
		this.dropMapper.clear();
		NBTTagList loot_mapper_obj = nbt.getTagList("loot_mapper", 10);
		for(int i=0; i < loot_mapper_obj.tagCount(); i++) {
			this.dropMapper.put(new ResourceLocationMeta(loot_mapper_obj.getCompoundTagAt(i).getString("key")),
					new ResourceLocationMeta(loot_mapper_obj.getCompoundTagAt(i).getString("value")));
		}
	}
	
	public void save(){
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			
			try {
				if(!filename.exists()) {
					filename.createNewFile();
				}
				NBTTagCompound nbttagcompound = new NBTTagCompound();
	            nbttagcompound.setTag("data", this.writeToNBT(new NBTTagCompound()));
	            FileOutputStream fileoutputstream = new FileOutputStream(filename);
	            CompressedStreamTools.writeCompressed(nbttagcompound, fileoutputstream);
	            fileoutputstream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void setNormal() {
		this.normal = true;
		this.save();
	}
	
	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setBoolean("normal", this.normal);
		NBTTagList loot_mapper_obj = new NBTTagList();
		for(ResourceLocationMeta key : this.dropMapper.keySet()) {
			NBTTagCompound obj = new NBTTagCompound();
			obj.setString("key", key.toString());
			obj.setString("value", this.dropMapper.get(key).toString());
			loot_mapper_obj.appendTag(obj);
		}
		compound.setTag("loot_mapper", loot_mapper_obj);
		return compound;
	}
}
