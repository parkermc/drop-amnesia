package com.parkermc.dropamnesia.droptools;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;

public class ResourceLocationMeta {
	private ResourceLocation resourceLocation;
	private int meta = 0;

	public ResourceLocationMeta(ResourceLocation resourceLocation) {
		this.resourceLocation = resourceLocation;
	}
	
	public ResourceLocationMeta(IBlockState state) {
		this(state.getBlock().getRegistryName());
		this.meta = state.getBlock().getMetaFromState(state);
	}
	
	public ResourceLocationMeta(ResourceLocation resourceLocation, int meta) {
		this(resourceLocation);
		this.meta = meta;
	}
	
	public ResourceLocationMeta(String str) {
		if(str.contains(":")) {
			String[] strSplit = str.split(":");
			if(strSplit.length == 2) {
				if(strSplit[1].matches("\\d+")) {
					this.resourceLocation = new ResourceLocation(strSplit[0]);
					this.meta = Integer.valueOf(strSplit[1]);
				}else {
					this.resourceLocation = new ResourceLocation(strSplit[0], strSplit[1]);
				}
			}else if(strSplit.length == 3) {
				this.resourceLocation = new ResourceLocation(strSplit[0], strSplit[1]);
				this.meta = Integer.valueOf(strSplit[2]);
			}
		}else {
			this.resourceLocation = new ResourceLocation(str);
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResourceLocationMeta other = (ResourceLocationMeta) obj;
		if (meta != other.meta)
			return false;
		if (resourceLocation == null) {
			if (other.resourceLocation != null)
				return false;
		} else if (!resourceLocation.equals(other.resourceLocation))
			return false;
		return true;
	}
	
	public int getMetadata() {
		return this.meta;
	}
	
	public ResourceLocation getResourceLocation() {
		return this.resourceLocation;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + meta;
		result = prime * result + ((resourceLocation == null) ? 0 : resourceLocation.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return this.resourceLocation.toString() + ":" + this.meta;
	}
}
