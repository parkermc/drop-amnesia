package com.parkermc.dropamnesia;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Config {
	public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public boolean silk_touch_drop_random = false;
	
	public List<List<String>> groups = new ArrayList<List<String>>();
	
	public Map<String,List<String>> block_grouping = new HashMap<String,List<String>>(); 
	
	public Config() {
	}
	
	public Config(boolean data){
		if(data) {
			groups.add(new ArrayList<String>());
			groups.get(0).add("_example");
			groups.get(0).add("_This example(once these comments are removed) would randomise the dimond_ore, iron_ore, and redstone_ore drops");
			groups.get(0).add("_To add blocks to a randomising group add the name to the group array like below");
			groups.get(0).add("_To add a new randomising group create a new array");
			groups.get(0).add("minecraft:diamond_ore");
			groups.get(0).add("minecraft:iron_ore");
			groups.get(0).add("minecraft:redstone_ore");
			
			block_grouping.put("_", new ArrayList<String>());
			block_grouping.get("_").add("Used to group blocks that drop the same thing but have different ids together in the randomising system");
			block_grouping.put("minecraft:redstone_ore", new ArrayList<String>());
			block_grouping.get("minecraft:redstone_ore").add("minecraft:lit_redstone_ore");
		}
	}
	
	public static Config Read(File file) throws IOException {
		if(!file.exists()) {
			file.createNewFile();
			Config obj = new Config(true);
			obj.Write(file);
			return obj;
		}
		return GSON.fromJson(new FileReader(file), Config.class);
		
	}
	
	public void Write(File file) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(GSON.toJson(this));
		writer.close();
	}
}
